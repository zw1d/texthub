# Texthub

Texthub is a  simple, databaseless link directory website written in PHP.

Side note: this is something I use everyday and also my first open source project. Hope it helps someone.


## Installation

1. Clone repository in a catalog reachable from web. For Apache in example:

    `cd /var/www/html/`
    
    `git clone https://gitlab.com/zw1d/texthub.git`

2. set correct owner:

    `sudo chown www-data texthub -R`

3. and permissions

    `sudo chmod 750 texthub -R` 

### Note

At the moment of release texthub requires `short_open_tag=On` in your `php.ini`.

## Usage

Texthub should be now available at `http://your.domain/texthub`. You will be asked to set your username and password.

There are current two kinds of users: `admin` and `user`. The latter cannot add or remove any links from the list.

At current state there's no way to add or remove users via the website. All modifications have to be made in `users.txt` (more on that below).

## Configuration files

Texthub stores all the information in two files, by default `hub.txt` and `users.txt`.

### users.txt structure
One user = one line

`user|sha1-hashed-password|role`

Example `users.txt`:

```
jack|40bd001563085fc35165329ea1ff5c5ecbdbbeef|admin
annie|5f6955d227a320c7f1f6c7da2a6d96a851a8118f|user
```

### hub.txt structure

One link = one line

`link|link-title|optional-icon-link`

Example `hub.txt`:

```
https://gitlab.com/zw1d/texthub|Texthub repo at gitlab
https://192.168.10.123|That accesspoint on the 3rd floor
http://my.domain:20123/admin|Admin dashboard of company PBX
https://google.com|Link to google that i can't remember|https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

All the code has to be contained within one index.php file.


## License
[GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/)
