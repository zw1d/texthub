<?php
   error_reporting(E_ALL);
   session_start();
   $_users_file = "users.txt";
   $imglinksperline=6;
   $a=array();
   $b=array();
   function validateLogin($user, $password, $hashedPassword = false) {
	  global $_users_file;
	  if(!is_file($_users_file)) {
		 echo "no users file (".$_users_file.")";
		 $file =	fopen($_users_file,'w');
		 fclose($file);
		 return false;
	  }
	  else {
		 $file =	fopen($_users_file,'r');
		 if($hashedPassword) {
			$calculatedHash = $password;
		 }
		 else {
			$calculatedHash = sha1($password);
		 }

		 while(($line = fgets($file)) !== false) {
			$l = array_filter(explode ('|', $line));
			foreach($l as $k => $v) {
			   $l[$k]=trim($v);
			}
			$userData = array('user'=>$l[0], 'hash'=>$l[1], 'role'=>$l[2]);
			if(strcasecmp($user,$userData['user'])==0) {
			   if(strcmp($userData['hash'], $calculatedHash) == 0) {
				  return $userData;
			   }
			   else {
				  return false;
			   }
			}
		 }
		 return false;
	  }
   }

   //Action mechanisms
   if($_SERVER['REQUEST_METHOD'] === 'POST') {
	  switch($_POST['action']) {
		 case 'login':
		 $userData = validateLogin($_POST['user'], $_POST['pass']);
		 if($userData!==false) {
			if(!empty($_POST['rememberme'])) {
			   setcookie('rememberme', serialize(array($userData['user'], $userData['hash'])), time()+(10*365*24*60*60), '/');
			}
			$_SESSION['login']=true; //Set login to true in case they got in via UN & PW
			foreach($userData as $key => $value) {
			   $_SESSION[$key]=$value;
			}
		 }
		 else {
			//wrong username or password
		 }
		 break;
		 case 'add':
		 $line = trim( $_POST['url'] ).'|'.trim( $_POST['name'] ).'|'.trim( $_POST['img'] )."\n";
		 echo file_put_contents('hub.txt', $line, FILE_APPEND );
		 break;
		 case 'delete':
		 $arr = file('hub.txt');
		 unset($arr[$_POST['line']]);
		 $arr = array_values($arr);
		 file_put_contents('hub.txt', implode($arr));
		 break;
		 case 'addUser':
		 $line = trim( $_POST['user'] ).'|'.trim( sha1($_POST['pass']) ).'|'.trim( (isset($_POST['isadmin']) && $_POST['isadmin'] != "")?'admin':'user' )."\n";
		 echo file_put_contents($_users_file, $line, FILE_APPEND );
		 break;
		 case 'logout':
		 setcookie('rememberme', null, -1, '/');
		 session_destroy();
		 break;
	  }
	  header("Location: ".$_SERVER['REQUEST_URI']); /* Redirect browser */
   }
?>
<!DOCTYPE html>
<html lang="en">
   <head>

	  <!-- Basic Page Needs
	  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	  <meta charset="utf-8">
	  <title>hub.txt</title>
	  <meta name="description" content="">
	  <meta name="author" content="zw1d">

	  <!-- Mobile Specific Metas
	  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	  <meta name="viewport" content="width=device-width, initial-scale=1">

	  <!-- FONT
	  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

	  <!-- CSS
	  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	  <link rel="stylesheet" href="css/normalize.css">
	  <link rel="stylesheet" href="css/skeleton.css">

	  <!-- Favicon
	  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	  <link rel="icon" type="image/png" href="images/favicon.png">
	  <style> body {
			   font-size:20px;
		 }
		 a {
			   text-decoration: none;
		 }
		 a:hover {
			   text-decoration: underline;
		 }
		 .linkImgs img{
			   max-width: 100%;
			   max-height: 140px;
		 }
		 .center {
			   text-align: center;
		 }
		 .section {
			   padding: 8rem 0 3rem;
		 }
		 .red {
			   color: red;
		 }
		 .right {
			   text-align: right;
		 }
		 .scroll-x {
			   overflow-x: scroll;
			   overflow: scroll;
		 }
		 .smaller {
			   font-size: 60%;
		 }
		 .nomargin {
			   margin:0;
		 }
		 .linkTitle {
			   text-overflow: ellipsis;
			   overflow: hidden;
			   display: block;
		 }
		 .linkUrl {
			   display: block;
			   overflow-x: auto;
			   overflow-y:hidden;
		 }
	  </style>
   </head>
   <body>
	  <div class="section">
		 <div class="container">
			<h1>hub.txt</h1>
		 </div>
	  </div>
	  <div class="section">
		 <div class="container">
			<?
			if(!isset($_SESSION['login']) && isset($_COOKIE['rememberme'])) {
			   $l = unserialize($_COOKIE['rememberme']);
			   $userData = validateLogin($l[0], $l[1], true);
			   if($userData !== false) {
				  $_SESSION['login']=true; //Set login to true in case they got in via UN & PW
				  foreach($userData as $key => $value) {
					 $_SESSION[$key]=$value;
				  }
			   }
			}?>
			<? if(filesize($_users_file) == 0) :?>
			<div class="row">
			   <h2>No users found in users file (<?=$_users_file;?>)</h2>
			   <form action="" method="post">
				  <div class="row">
					 <input type="hidden" name="action" value="addUser">
					 <label for="user">New user login</label>
					 <input type="text" name="user" id="user">
					 <label for="pass">New user password</label>
					 <input type="password" name="pass" id="pass"> 
				  </div> 
				  <div class="row">
					 <? if(filesize($_users_file) == 0): ?>
					 <label>
						<input type="checkbox" name="dummy" value="1" disabled checked="checked">
						Admin priviliges
					 </label>
					 <input type="hidden" name="isadmin" value="1">
					 <?else: //user this when user form will get wrapped in some funciton?>
					 <?endif;?>
				  </div>
				  <input type="submit" value="Create user" name="password">
				</form> 
			 </div> 
			 <? elseif(!isset($_SESSION['login']) || $_SESSION['login'] == false) :?>
			<div class="row">
			   <h2>Logowanie</h2>
			   <form action="" method="post">
				  <div class="row">
					 <input type="hidden" name="action" value="login">
					 <label for="login">Login</label>
					 <input type="text" name="user" id="user">
					 <label for="pass">Password</label>
					 <input type="password" name="pass" id="pass">
				  </div>
				  <div class="row">
					 <label>
						<input type="checkbox" name="rememberme" value="1">
						Don't ask again
					 </label>
				  </div>
				  <input type="submit" value="login" name="password">
			   </form>	
			</div>
			<? else: ?>
			   <?if(!is_file('hub.txt')) {
				  echo "no file";
				  $file =	fopen('hub.txt','w');
				  fclose($file);
			   }
			   else {
				  $file =	fopen('hub.txt','r');
				  $i=0;
				  while(($line = fgets($file)) !== false) {
					 $l = array_filter(explode ('|', $line));
					 foreach($l as $key => $value) {
						if(!empty(trim($value))) {#if trimmed value is not empty
						   $l[$key]=trim($value);
						}
						else {
						   unset($l[$key]);
						}
					 }
					 $l['line']=$i;
					 if(count($l)>3) {
						$a[]=$l; #with images
					 }
					 else {
						$b[]=$l; #without images
					 }
					 $i++;
				  }
			   }
			?>
			<?if(count($a) == 0 && count($b) == 0):?>
			<div class="row">
			   <h3>hub.txt is empty</h3>
			</div>
			<?endif;?>
			<?if(count($a)): ?>
			   <?for($j=0; $j < ceil(count($a)/$imglinksperline); $j++) : ?>
			   <div class="row">
				  <?for($i=0; $i < $imglinksperline; $i++):?>
				  <?
				  if(array_key_exists($i+$j*$imglinksperline,$a)) {
					 $l = $a[$i+$j*$imglinksperline];
				  }
				  else {
					 break;
				  }
			   ?>
					 <div class="two columns linkImgs center">
						<a target="_blank" title="<?=$l[1];?>" href="<?=$l[0];?>">
						   <img class="" src="<?=$l[2];?>" alt="<?=$l[1];?>">
						</a>
						<div class="row">
						   <a target="_blank" class="linkTitle" title="<?=$l[1];?>" href="<?=$l[0];?>">
							  <?=$l[1];?>
						   </a>
						</div>
						<? if(strcmp($_SESSION['role'], 'admin') == 0) :?>
						<form action="" method="post" class="nomarign">
						   <input type="hidden" name="action" value="delete">
						   <input type="hidden" name="line" value="<?=$l['line'];?>">
						   <button onclick="return confirm('Usunac?');" type="submit"  class="button imgX red">
							  x
						   </button>
						</form>
						<?endif;?>
					 </div>
				  <?endfor;?>
			   </div>
			   <?endfor;?>
			<?else: //no image links?>

			<?endif;?>

		 </div>
	  </div>
	  <div class="section">
		 <div class="container">
			<div class="row">
			   <?if(count($b)): for($i=0; $i < count($b); $i++): $l = $b[$i];?>
			   <div class="row">
				  <div class="seven columns">
					 <a target="_blank" class="linkTitle" href="<?=$l[0];?>" title="<?=$l[1];?>"><?=$l[1];?></a>
				  </div>
				  <div class="three columns">
					<code class="scroll-x smaller linkUrl">
						<?=$l[0];?>
					 </code>
				  </div>
				  <div class="two columns right">
					 <? if(strcmp($_SESSION['role'], 'admin') == 0) :?>
					 <form action="" method="post" class="nomargin">
						<input type="hidden" name="action" value="delete">
						<input type="hidden" name="line" value="<?=$l['line'];?>">
						<button onclick="return confirm('Usunac?');" type="submit"  class="button imgX red">
						   x
						</button>
					 </form>
					 <?endif;?>
				  </div>
			   </div>
			   <?endfor; else:?>

			   <?endif;?>
			</div>
		 </div>
	  </div>
	  <div class="section">
		 <div class="container">
			<div class="row">
			   <? if(strcmp($_SESSION['role'], 'admin') == 0) : ?>
			<h2>Add new links</h2>
			<form action="" method="post" >
			   <input type="hidden" name="action" value="add">
			   <div class="row">
				  <div class="six columns">
					 <label for="linkName">Link name</label>
					 <input type="text" name="name" id="linkName" placeholder="name" class="u-full-width">
				  </div>
				  <div class="six columns">
					 <label for="imgUrl">Image URL</label>
					 <input type="text" name="img" placeholder="img-url" id="imgUrl" class="u-full-width">
				  </div>
			   </div>
			   <label for="linkUrl">Link URL</label>
			   <input type="text" name="url" placeholder="url" id="linkUrl" class="u-full-width">
			   <input type="submit" name="add" value="add" class="button button-primary">
			</form>
			<?endif;?>
			<?endif;?>
			<?if(isset($_SESSION['login']) && $_SESSION['login'] == true) :?>
			<form action="" method="post">
			   <input type="hidden" name="action" value="logout">
			   <button type="submit" class="button">Logout <?=$_SESSION['user'];?></button>
			</form>
			<?endif;?>
		 </div>
	  </div>
   </div>
   <div class="section">
	  <div class="container">
		 <div style="text-align: center; "><a style="font-size: 60%; color:#000;" href="https://gitlab.com/zw1d/texthub">texthub</a></div>
	  </div>
   </div>
   </body>
</html>
